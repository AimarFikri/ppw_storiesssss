from django.shortcuts import render, redirect
from django.views.generic import DetailView


from .forms import ActivityForm,PersonForm

from .models import Activity,Person

# Create your views here.
def kegiatan_list(request):
    form_kegiatan = ActivityForm()
    kegiatanList = Activity.objects.all()
    form_orang = PersonForm()
    if request.method == "POST":
        form_kegiatan = ActivityForm(request.POST)
        form_orang = PersonForm(request.POST)
        if form_orang.is_valid():
            form_orang.save()
            return redirect('/Kegiatan')
        if form_kegiatan.is_valid():
            form_kegiatan.save()
            return redirect('/Kegiatan')

        
    
    context = {
        "kegiatan_list" : kegiatanList,
        "form_orang" : form_orang,
        'form_kegiatan' : form_kegiatan
    }
    return render(request, 'Kegiatan.html', context)



def kegiatan_delete(request, pk):
    kegiatanInput = Activity.objects.get(id=pk)
    if request.method == "POST":
        kegiatanInput.delete()
        return redirect('/Kegiatan')

def orang_delete(request, pk):
    orangInput = Person.objects.get(id=pk)
    if request.method == "POST":
        orangInput.delete()
        return redirect('/kegiatanList')

