from django import  forms

from .models import Activity, Person

class ActivityForm(forms.ModelForm):
    class Meta: 
        model = Activity
        fields=('name',)
        labels={
            'name':'nama',
        }
        widgets = {'name' : forms.TextInput( attrs = {
        'class':'form-control ',
        'placeholder' : 'nama Matkul',
        })}

class PersonForm(forms.ModelForm):
    class Meta:
        model=Person
        fields="__all__"