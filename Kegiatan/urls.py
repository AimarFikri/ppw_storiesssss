from django.urls import path
from . import views



 
urlpatterns = [
    path('', views.kegiatan_list, name='kegiatan'),
    path('addorang/', views.kegiatan_list, name='Orang_add'),
    path('delete/<str:pk>', views.kegiatan_delete,name='kegiatan_delete'),
    path('orangDelete/<str:pk>/', views.orang_delete, name="orang_delete"),
]
