from django.db import models


# Create your models here.
class Activity(models.Model):
    name = models.CharField(default = "Kegiatan (-_-)", max_length=50)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Activities"

class Person(models.Model):
    name=models.CharField( max_length=50)
    Kegiatan = models.ForeignKey("Activity", on_delete=models.CASCADE)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural="People"